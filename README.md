# Fediverse-scripts

Bunch of scripts to play with the fediverse

## check-broken-follow-pages.pl 

checks who you follow and if those accounts still work.

add your follow page as an argument, you can then copypaste the uid's the mastodon search-bar to find and unfollow the users.

Warning: this script isn't fool proof and sometimes an instance just has a hickup, so double check the account/instance are still there.

    
